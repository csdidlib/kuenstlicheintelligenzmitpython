import requests

def classify(text):

  key = "Tragt hier den API Key eures Projekts ein."
  url = "https://machinelearningforkids.co.uk/api/scratch/" + key + "/classify"
  response = requests.get(url, params={ "data" : text })

  if response.ok:
    responseData = response.json()
    topMatch = responseData[0]
    return topMatch
  else:
    response.raise_for_status()

def answer_question():

  eingabe = input("Frage mich etwas...\n")
  klassifikation = classify(eingabe)
  kategorie = klassifikation["class_name"]
  
  if kategorie == "Essen":
    print("Was Eulen essen, willst du wissen? Das hängt von der Eulenart ab. Kleine Eulen essen zum Beispiel
wirbellose Tiere. Dazu zählen Spinnen, Insekten und Würmer. Größere Eulen essen auch Tiere wie Fische, Vögel usw.
...")
  elif kategorie == "Lebensraum":
    print("Wo Eulen leben, möchtest du wissen? Es gibt fast überall auf der Welt Eulen. Manche leben in der
Wüste, andere im Wald, und wieder andere an ganz anderen Flecken auf der Erde. ...")
  elif kategorie == "Arten":
    print("Dich interessiert, welche Arten von Eulen es gibt? Es gibt diverse Eulenarten. Manche sind kleiner als
deine Hand, andere so groß wie dein Schulrucksack. Insgesamt gibt es über 200 Spezies. ...")
  elif kategorie == "Merkmale":
    print("Du willst wissen, woran du eine Eule erkennst? Wie alle Vögel haben Eulen einen Schnabel. Allerdings
liegt dieser sehr nah am Kopf an. Charakteristisch ist außerdem, dass Eulen ihren Kopf sehr weit drehen können.
...")
  elif kategorie == "Lebensdauer":
    print("Hmm.. wie alt werden Eulen= Ich kann mich nicht mal erinnern, wie alt ich bin. ...")

print("Hallo! Möchtest du etwas über Eulen erfahren?")

while True:
  answer_question()

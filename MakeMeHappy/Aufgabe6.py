import time
import requests

def classify(text):
  key = "Tragt hier den API Key (mit Anfuehrungszeichen) eures Projekts ein."
  url = "https://machinelearningforkids.co.uk/api/scratch/" + key + "/classify"

  response = requests.get(url, params={ "data" : text })

  if response.ok:
    responseData = response.json()
    topMatch = responseData[0]
    return topMatch
  else:
    response.raise_for_status()

def answer_question():
  eingabe = input("Schreibe etwas...\n")
  klassifikation = classify(eingabe)
  kategorie = klassifikation["class_name"]

  if kategorie == "Nett":
    print(":-)")
  elif kategorie == "Gemein":
    print(":-(")
  else:
    print("Keine Klassifikation moeglich.\n")

  time.sleep(3)
  print(":-|")

print(":-|")

while True:
  answer_question()
